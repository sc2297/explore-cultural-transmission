# Sampling distribution is multi normal. Prior is banana-shape.

rm(list = ls())

library(abc)
library(mvtnorm)
library(ranger)
# sampling distribution: multi normal
p <- 15
Sigma <- diag(1, p)
data_obs <- c(10, rep(0, p-1))
#obs_stats <- c(colMeans(data_obs), apply(data_obs, 2, var))
obs_stats <- data.frame(t(data_obs))

# banana shape prior
n_sim <- 1000000
b <- 0.1
A <- diag(c(100, rep(1, p-1))) # Covariance
sum_stats <- matrix(0, n_sim, p)
theta_prop <- mvrnorm(n_sim, rep(0, p), A)
theta_prop[, 2] <- theta_prop[, 2] + b * theta_prop[, 1]^2 - 100 * b #transform
for(i in 1:n_sim){
  data_sim <- mvrnorm(1, theta_prop[i, ], Sigma)
  sum_stats[i, ] <- data_sim
}

# abc
model.rej = abc(obs_stats, param = theta_prop, sumstat = sum_stats, tol = 0.01, method = "rejection")
plot(model.rej$unadj.values[,1], model.rej$unadj.values[,2], type = 'p')




model.loc = abc(obs_stats, param = theta_prop, sumstat = sum_stats, tol = 0.01, method = "loclinear")
plot(model.loc$adj.values[,1], model.loc$adj.values[,2], type = 'p')

model.neu = abc(obs_stats, param = theta_prop, sumstat = sum_stats, tol = 0.01, method = "neuralnet")
plot(model.neu$adj.values[,1], model.neu$adj.values[,2], type = 'p')

model.ri = abc(obs_stats, param = theta_prop, sumstat = sum_stats, tol = 0.01, method = "ridge")
plot(model.ri$adj.values[,1], model.ri$adj.values[,2], type = 'p')


### the function of ABC-RFA
abcrfa=function(target, param, sumstat, tol){
  model = abc(target, param = param, sumstat = sumstat, tol, method = "loclinear")
  weights=model$weights
  unadj.values=model$unadj.values
  N=dim(sumstat)[1]
  subnumber=c(1:N)[model$region]
  wei=weights/sum(weights)
  #### randomforest regression adjustment
  re=c(which(wei==0))
  againwei=wei[-re]
  subnumber=c(1:N)[model$region][-re]
  npara=dim(param)[2]
  adj.values=matrix(1,length(subnumber),npara)
  for(i in 1:npara){
    data=data.frame(param[,i],sumstat)
    names(data)[1]<-'param'
    tree.model <- ranger(param~.,data=data[subnumber,],case.weights = againwei)
    pred=1:length(againwei)
    for(k in 1:length(againwei)){
      if(tree.model$predictions[k]=="NaN"){
        pred[k]=predict(tree.model,data[subnumber,][k,-1])$predictions
      }
      else{
        pred[k]=tree.model$predictions[k]
      }
    } 
    
    residuals=data[subnumber,1]-pred
    names(target)=names(data)[-1]
    adj.values[,i]=predict(tree.model,data=target)$predictions+residuals
  }
  return(list(adj.values=adj.values,unadj.values=unadj.values))
}

# rfa
model.rfa <- abcrfa(obs_stats, param = theta_prop, sumstat = sum_stats, tol = 0.01)
plot(model.rfa$adj.values[,1], model.rfa$adj.values[,2], type = 'p')

theta1_post=model.rej$unadj.values[,1]
theta2_post=model.rej$unadj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)

par(mfrow=c(1,1))
par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(2,14),ylim=c(-10,10),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue")###main='rejection',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))



theta1_post=model.loc$adj.values[,1]
theta2_post=model.loc$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)
par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue",add=T)###main='loclinear',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1),col="red")



theta1_post=model.rfa$adj.values[,1]
theta2_post=model.rfa$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)
par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue")###main='abcrfa',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))


theta1_post=model.neu$adj.values[,1]
theta2_post=model.neu$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)

par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue")###main='neuralnet',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))


theta1_post=model.ri$adj.values[,1]
theta2_post=model.ri$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)

par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col = 'blue')###main='ridge',
contour(f1, add = T,lwd=2,drawlabels = FALSE,nlevels = 10,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))





allres=readRDS("abcRF150k_NMU.RDS")
alldismulti=allres[[1]]
betas=allres[[2]]
Js=allres[[3]]
Ns=allres[[4]]
mus=allres[[5]]
tsteps=allres[[6]]
disfunc=names(alldismulti[1,][[1]])
names(disfunc)=disfunc
obs=as.data.frame(t(sapply(disfunc,function(i)0)))
artifc=lapply(disfunc,function(i)sapply(alldismulti[2,],"[[",i))
artifc=sapply(artifc,function(i)i[5,])
artifc=apply(artifc,2,as.numeric)
param=cbind.data.frame(beta=betas,J=Js,N=Ns,mu=mus,t=tsteps)


# abc
model.rej = abc(obs, param = param, sumstat = artifc, tol = 0.005, method = "rejection")
plot(model.rej$unadj.values[,2], model.rej$unadj.values[,1], type = 'p',xlim=c(0,2),ylim=c(0,2))

#model.loc = abc(obs, param = param, sumstat = artifc, tol = 0.01, method = "loclinear")
#plot(model.loc$unadj.values[,1], model.loc$unadj.values[,2], type = 'p')
#
#model.neu = abc(obs, param = param, sumstat = artifc, tol = 0.005, method = "neuralnet")
#plot(model.neu$adj.values[,1], model.neu$adj.values[,2], type = 'p')
#
#model.ri = abc(obs_stats, param = theta_prop, sumstat = sum_stats, tol = 0.01, method = "ridge")
#plot(model.ri$adj.values[,1], model.ri$adj.values[,2], type = 'p')
cols=RColorBrewer::brewer.pal(8,"Set2")
    names(cols)=names(originalparams)

    layout(mat=t(c(1,2)),widths=c(.65,.35))
    par(mar=c(4,4,2,0))
    plot(1,1,ylim=c(0,2),xlim=c(0,2),type="n",xlab="J",ylab=expression(beta))
    allmodes=lapply(c(1:5,7),function(mod){
        artifc=lapply(disfunc,function(i)sapply(alldismulti[2,],"[[",i))
        artifc=sapply(artifc,function(i)i[mod,])
        artifc=apply(artifc,2,as.numeric)
        model.rfa <- abcrfa(obs, param = param, sumstat = artifc, tol = 0.005)
        cor=model.rfa$adj.values[,2]>0 & model.rfa$adj.values[,1]>0
        points(model.rfa$adj.values[cor,2], model.rfa$adj.values[cor,1], type = 'p',col=adjustcolor(cols[[mod]],.4),pch=20,cex=2)
        mb=hdrcde::hdr(model.rfa$adj.values[cor,2])$mode
        mJ=hdrcde::hdr(model.rfa$adj.values[cor,1])$mode
        c(mb,mJ)
    })
    names(allmodes)=names(originalparams)[c(1:5,7)]
        artifc=sapply(disfunc,function(i)sapply(alldismulti[1,],"[[",i))
        artifc=apply(artifc,2,as.numeric)
        model.rfa <- abcrfa(obs, param = param, sumstat = artifc, tol = 0.005)
        cor=model.rfa$adj.values[,2]>0 & model.rfa$adj.values[,1]>0
        points(model.rfa$adj.values[cor,2], model.rfa$adj.values[cor,1], type = 'p',bg=adjustcolor("grey",.4),pch=21,cex=1)
        mb=hdrcde::hdr(model.rfa$adj.values[,2])$mode
        mJ=hdrcde::hdr(model.rfa$adj.values[,1])$mode
        points(mb,mJ,cex=4,bg=adjustcolor("grey",.8),pch=21,col="red")
    #lapply(gpe[[1]],function(n)points(Js[allbests[[n]][,met]],betas[allbests[[n]][,met]])
    lapply(names(allmodes),function(i)points(allmodes[[i]][1],allmodes[[i]][2],bg=adjustcolor(cols[[i]],.8),pch=21,cex=4,col=adjustcolor("red",.5)))
    lapply(c(1:5,7),function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=adjustcolor(cols[[i]],.8),pch=21,cex=4))
    #legend("topleft",legend=1:5,pch=20,col=adjustcolor(cols[gpe[[1]]],.6),pt.cex=2,bg="white",ncol=2)
    par(mar=c(4,0,2,0))
    plot(rep(1,6),3:8,bg=rev(cols[c(1:5,7)]),pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    text(rep(1.2,6),3:8,as.expression(lapply(rev(c(1:5,7)),function(m)bquote(list(J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)

    png(paste0(projfol,"2Dposterios_RFA.png"),width=1000,height=600,pointsize=15)
    layout(mat=t(c(1,2)),widths=c(.65,.35))
    par(mar=c(4,4,2,0))
    plot(1,1,ylim=c(0,.8),xlim=c(0,1.5),type="n",xlab="J",ylab=expression(beta))
    lapply(c(1:5,7),function(mod){
        artifc=lapply(disfunc,function(i)sapply(alldismulti[2,],"[[",i))
        artifc=sapply(artifc,function(i)i[mod,])
        artifc=apply(artifc,2,as.numeric)
        model.rfa <- abcrfa(obs, param = param, sumstat = artifc, tol = 0.005)
        cor=model.rfa$adj.values[,2]>0 & model.rfa$adj.values[,1]>0
        points(model.rfa$adj.values[cor,2], model.rfa$adj.values[cor,1], type = 'p',col=adjustcolor(cols[[mod]],.4),pch=20,cex=2)
    })
    lapply(c(1:5,7),function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=adjustcolor(cols[[i]],.8),pch=21,cex=4))
    #legend("topleft",legend=1:5,pch=20,col=adjustcolor(cols[gpe[[1]]],.6),pt.cex=2,bg="white",ncol=2)
    par(mar=c(4,0,2,0))
    plot(rep(1,6),3:8,bg=rev(cols[c(1:5,7)]),pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    text(rep(1.4,6),3:8,as.expression(lapply(rev(c(1:5,7)),function(m)bquote(list(J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
    dev.off()


    png(paste0(projfol,"2Dposterios_RFA_estimates.png"),width=1000,height=600,pointsize=15)
    layout(mat=t(c(1,2)),widths=c(.65,.35))
    par(mar=c(4,4,2,0))
    plot(1,1,ylim=c(0,.6),xlim=c(0,1.5),type="n",xlab="J",ylab=expression(beta))
    allmodes=lapply(names(models),function(mod){
        artifc=lapply(disfunc,function(i)sapply(alldismulti[2,],"[[",i))
        artifc=sapply(artifc,function(i)i[mod,])
        artifc=apply(artifc,2,as.numeric)
        model.rfa <- abcrfa(obs, param = param, sumstat = artifc, tol = 0.005)
        cor=model.rfa$adj.values[,2]>0 & model.rfa$adj.values[,1]>0
        points(model.rfa$adj.values[cor,2], model.rfa$adj.values[cor,1], type = 'p',col=adjustcolor(cols[[mod]],.4),pch=20,cex=2)
        mb=hdrcde::hdr(model.rfa$adj.values[cor,2])$mode
        mJ=hdrcde::hdr(model.rfa$adj.values[cor,1])$mode
        c(mb,mJ)
    })
    names(allmodes)=names(originalparams)[c(1:5,7)]
    lapply(names(allmodes),function(i)points(allmodes[[i]][2],allmodes[[i]][1],bg=adjustcolor(cols[[i]],.9),pch=21,cex=4,col=adjustcolor("red",.8)))
    par(mar=c(4,0,2,0))
    plot(rep(1,6),3:8,bg=rev(cols[c(1:5,7)]),pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    text(rep(1.4,6),3:8,as.expression(lapply(rev(c(1:5,7)),function(m)bquote(list(J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
    dev.off()

    png(paste0(projfol,"2Dposterios_RFA_estimates+known.png"),width=1000,height=600,pointsize=15)
    layout(mat=t(c(1,2)),widths=c(.65,.35))
    par(mar=c(4,4,2,0))
    plot(1,1,ylim=c(0,2),xlim=c(0,2),type="n",xlab="J",ylab=expression(beta))
    allmodes=lapply(c(1:5,7),function(mod){
        artifc=lapply(disfunc,function(i)sapply(alldismulti[2,],"[[",i))
        artifc=sapply(artifc,function(i)i[mod,])
        artifc=apply(artifc,2,as.numeric)
        model.rfa <- abcrfa(obs, param = param, sumstat = artifc, tol = 0.005)
        cor=model.rfa$adj.values[,2]>0 & model.rfa$adj.values[,1]>0
        points(model.rfa$adj.values[cor,2], model.rfa$adj.values[cor,1], type = 'p',col=adjustcolor(cols[[mod]],.4),pch=20,cex=2)
        mb=hdrcde::hdr(model.rfa$adj.values[cor,2])$mode
        mJ=hdrcde::hdr(model.rfa$adj.values[cor,1])$mode
        c(mb,mJ)
    })
    names(allmodes)=names(originalparams)[c(1:5,7)]
    lapply(c(1:5,7),function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=adjustcolor(cols[[i]],.8),pch=21,cex=4))
    lapply(names(allmodes),function(i)points(allmodes[[i]][1],allmodes[[i]][2],bg=adjustcolor(cols[[i]],.8),pch=21,cex=4,col=adjustcolor("red",.8)))
    par(mar=c(4,0,2,0))
    plot(rep(1,6),3:8,bg=rev(cols[c(1:5,7)]),pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    text(rep(1.4,6),3:8,as.expression(lapply(rev(c(1:5,7)),function(m)bquote(list(J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
    dev.off()



    png(paste0(projfol,"2Dposterios_RFA_LBK.png"),width=1000,height=600,pointsize=15)
    layout(mat=t(c(1,2)),widths=c(.65,.35))
    par(mar=c(4,4,2,0))
    plot(1,1,ylim=c(0,2),xlim=c(0,2),type="n",xlab="J",ylab=expression(beta))
    lapply(c(1:5,7),function(mod){
        artifc=lapply(disfunc,function(i)sapply(alldismulti[2,],"[[",i))
        artifc=sapply(artifc,function(i)i[mod,])
        artifc=apply(artifc,2,as.numeric)
        model.rfa <- abcrfa(obs, param = param, sumstat = artifc, tol = 0.005)
        cor=model.rfa$adj.values[,2]>0 & model.rfa$adj.values[,1]>0
        points(model.rfa$adj.values[cor,2], model.rfa$adj.values[cor,1], type = 'p',col=adjustcolor(cols[[mod]],.4),pch=20,cex=2)
    })
    #legend("topleft",legend=1:5,pch=20,col=adjustcolor(cols[gpe[[1]]],.6),pt.cex=2,bg="white",ncol=2)
    artifc=sapply(disfunc,function(i)sapply(alldismulti[1,],"[[",i))
    artifc=apply(artifc,2,as.numeric)
    model.rfa <- abcrfa(obs, param = param, sumstat = artifc, tol = 0.005)
    cor=model.rfa$adj.values[,2]>0 & model.rfa$adj.values[,1]>0
    points(model.rfa$adj.values[cor,2], model.rfa$adj.values[cor,1], type = 'p',bg=adjustcolor("grey",.4),pch=21,cex=1)
    mb=hdrcde::hdr(model.rfa$adj.values[,2])$mode
    mJ=hdrcde::hdr(model.rfa$adj.values[,1])$mode
    lapply(c(1:5,7),function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=adjustcolor(cols[[i]],.8),pch=21,cex=4))
    points(mb,mJ,cex=4,bg=adjustcolor("grey",.8),pch=21,col="red")
    par(mar=c(4,0,2,0))
    plot(rep(1,7),2:8,bg=c("grey",rev(cols[c(1:5,7)])),pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    text(rep(1.4,7),2:8,as.expression(c("LBK",lapply(rev(c(1:5,7)),function(m)bquote(list(J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta)))))),adj=0)
    dev.off()


theta1_post=model.rej$unadj.values[,1]
theta2_post=model.rej$unadj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)

par(mfrow=c(1,1))
par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(2,14),ylim=c(-10,10),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue")###main='rejection',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))



theta1_post=model.loc$adj.values[,1]
theta2_post=model.loc$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)
par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue")###main='loclinear',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))



theta1_post=model.rfa$adj.values[,1]
theta2_post=model.rfa$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)
par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue")###main='abcrfa',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))


theta1_post=model.neu$adj.values[,1]
theta2_post=model.neu$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)

par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col="blue")###main='neuralnet',
contour(f1, add = T,lwd=2,drawlabels = FALSE,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))


theta1_post=model.ri$adj.values[,1]
theta2_post=model.ri$adj.values[,2]
grid_n = 100
f1 <- kde2d(theta1_post, theta2_post, n=grid_n)
post.true <- matrix(0, grid_n, grid_n)
for(i in 1:grid_n){
  for(j in 1:grid_n){
    seta1=f1$x[i]
    seta2=f1$y[j] - b * f1$x[i]^2 + 100 * b
    post.true[i, j] = exp(-(data_obs[1]-seta1)^2/2-(data_obs[2]-f1$y[j])^2/2-seta1^2/200-seta2^2/2)
  }
}
f2 <- list(x = f1$x, y = f1$y, z = post.true)

par(mar=c(5,5,4,2))
contour(f2,lty=2,xlim=c(7,12),ylim=c(-3,3),xlab=expression(theta[1]),ylab=expression(theta[2]),cex.axis=1.1,cex.lab=1.1,drawlabels = FALSE,col = 'blue')###main='ridge',
contour(f1, add = T,lwd=2,drawlabels = FALSE,nlevels = 10,levels = max(f1$z)*seq(from=0.1,to=0.9,by=0.1))


