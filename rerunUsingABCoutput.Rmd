Rerun test simulation

```{r}
source("functions.R")

m=5
u0="runif"
tstep=200
N=1000
p0=as.numeric(table(sample(size=N,1:m,replace=T)))
u0="rnorm"
mu=.1

library(parallel)

exp=list(LBK="allLBKLOGNORM")
projfol=exp[[1]]

dataset=list(LBK="data/ceramics_lbk_merzbach.RDS")
data=readRDS(dataset[[1]])
originalruns=readRDS("artificialmodels/backup_artificalruns.RDS")$runs
originalruns=lapply(originalruns,subsample,model=data)
originalruns=lapply(originalruns,"[[","freq")
originalruns[["LBK"]]=data
allmodes=readRDS("allLBKLOGNORM/LBK_allposteriors.RDS")
nrerun=100
params=lapply(allmodes,function(i)sample(nrow(i),nrerun,replace=T))

cl <- makeCluster(spec = 7,"FORK")
allrerruns=parLapply(cl,names(allmodes),function(mode)sapply(params[[mode]],function(p)subsample(model(p0=p0,J=allmodes[[mode]][p,1],u0=u0,beta=allmodes[[mode]][p,2],sde=1,tstep=tstep,mu=mu,N=N,m=m,mutate=T),data)$freq,simplify = F))
stopCluster(cl)
names(allrerruns)=names(allmodes)
#saveRDS(file="100reruns.RDS",allrerruns)
allrerruns=readRDS(file="5000reruns.RDS")

cols=RColorBrewer::brewer.pal(length(originalruns)-1,"Set2")
cols[[length(cols)+1]]="grey"
names(cols)=names(originalruns)

metrics <- c("d.sim", "d.gap" , "d.turn", "d.unique", "d.gini","d.lognormsd","d.lognormmean")
namesmetrics <- c("d.sim"="evenness", "d.gap"="disparity" , "d.turn"="turnover", "d.unique"="richness", "d.gini"="inequality","d.lognormsd"="log normal deviation","d.lognormmean"="log normal mean")
allsummarize=list()

library(parallel)
##get allsumary metrics
#for(m in names(allrerruns)){
#    allsummarize[[m]]=list()
#    for(i in metrics){
#        allsummarize[[m]][[i]]=list()
#        nt=get(i)(originalruns[[m]])
#        print(paste(m,i))
#        st=Sys.time()
#        if(grepl("log",i)){
#            cl <- makeCluster(spec = 10,"FORK",outfile="posteriorCheck.log")
#            allsummarize[[m]][[i]]=apply(parSapply(cl,allrerruns[[m]],function(frq)get(i)(frq)),1,function(i)i,simplify=F)
#            stopCluster(cl)
#        }
#        else allsummarize[[m]][[i]]=apply(sapply(allrerruns[[m]],function(frq)get(i)(frq)),1,function(i)i,simplify=F)
#    }
#
#}
#saveRDS(file="allmetricsReruns.RDS",allsummarize)

# print all 
originalparams=readRDS("artificialmodels/backup_artificalruns.RDS")$parameters
originalparams[[names(exp)]]$J="?"
originalparams[[names(exp)]]$beta="?"
for(m in names(allsummarize)){
    png(file.path(projfol,paste0(names(exp),"_posterior_check_",m,".png")),width=1400,height=900,pointsize=22)
    par(mfrow=c(2,3))
    par(oma=c(0,0,2,0))
    for(i in metrics[-3]){
        nt=get(i)(originalruns[[m]])
        nt[nt<0]=0
        simres=allsummarize[[m]][[i]]
        simres=lapply(simres,function(i){i[i<0]=0;i})
        if(grepl("log",i)) hdr.boxplot(simres,col=cols[[m]],main=paste(namesmetrics[i]),xaxt="n",outline=F,prob=c(75,95),ylim=c(0,7))
        else hdr.boxplot(simres,col=cols[[m]],main=paste(namesmetrics[i],"for",m),xaxt="n",outline=F,prob=c(75,95))
        axis(1,at=1:ncol(data),label=colnames(data))
        points(nt,col="red",lwd=2,pch=5)
    }
    mtext(text=bquote(list(paste("results for scenario ",.(m)),J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$J))),side=3,outer=T,font=2)
    dev.off()

}
