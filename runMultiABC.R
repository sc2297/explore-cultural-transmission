source("../R/functions.R")


dataset=list(LBK="data/ceramics_lbk_merzbach.RDS")

allart=readRDS("artificialmodels/backup_artificalruns.RDS")$runs

ns=200

Js=runif(ns,0,2) #we generate random value of J
betas=runif(ns,0,2) #we generate random value of beta
Ns=round(10^runif(ns,2,4)) #we generate random value of N
mus=runif(ns,0.06,0.12) #we generate random value of mu
tsteps=round(runif(ns,200,400)) #we generate random value of tstep
data=readRDS(dataset[[1]])
m=5
u0="rnorm"

allsubamp=lapply(allart,function(i)subsample(i,data))
allsetups=lapply(allsubamp,"[[","freq")
allsetups[["data"]]=data

metrics <- c(d.sim, d.gap , d.turn, d.spec, d.unique, d.gini,d.lognormmean,d.lognormsd)
names(metrics) <- c("d.sim", "d.gap" , "d.turn", "d.spec", "d.unique", "d.gini","d.lognormsd","d.lognormmean")

metrics=metrics[c(1,2,3,5,6,7,8)]

allsummetrics=lapply(names(metrics),function(d)lapply(names(allsetups),function(i){print(paste(d,i));get(d)(allsetups[[i]])}))
names(allsummetrics)=names(metrics)
library(parallel)
cl <- makeCluster(spec = 40,"FORK",outfile=paste0(names(dataset),".log"))
alldismulti=parLapply(cl,1:ns,function(i) {
                          tryCatch({
                              if(i%%5==0)print(i)
                              J=Js[i]
                              beta=betas[i]
                              N=Ns[i]
                              mu=mus[i]
                              tstep=tsteps[i]
                              p0=as.numeric(table(sample(size=N,1:m,replace=T)))
                              #we use the random value of beta and J and run the model
                              #print("run simu")
                              res=model(p0=p0,J=J,u0=u0,beta=beta,sde=1,tstep=tstep,mu=mu,N=N,m=m,mutate=T,log=F)
                              #we apply the same process of subsampling than to our fake data (checking for some case where we don't have data  we 
                              ssres=subsample(res,data)
                              #print("compute dist")
                              if(length(dim(ssres$freq))<1){
                                  print(ssres)
                                  return(c(NA,NA))
                              }
                              else{
                                  #compute the metrics
                                  modelmetrics=lapply(metrics,function(m)m(ssres$freq))
                                  #we then compute the distance between simulation and every models
                                  distances=lapply(names(metrics),function(d)lapply(allsummetrics[[d]],function(m)sqrt(sum(m-modelmetrics[[d]])^2)))
                                  names(distances)=names(metrics)
                                  return(distances)
                              }
                          },error=function(i){
                              print("=======pb======")
                              if(exists("sdres")){
                                  print(typeof(ssres))
                                  print(typeof(res))
                                  print(dim(ssres))
                                  print(dim(res))
                                  print(length(ssres))
                                  print(dim(res))
                              }
                              else{
                                  print("no res")
                              }
                              print("=======endpb======")
                              return(paste0("problem with this",i))
                          })
                      }
)
stopCluster(cl)

abcdata=list(prior=list(J=Js,beta=betas,N=Ns,mu=mus,tstep=tsteps),metrics=alldismulti)
saveRDS(file=paste0(names(dataset),"_results.RDS"),abcdata)


